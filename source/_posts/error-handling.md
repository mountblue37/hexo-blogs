---
title: Error Handling with Promises
date: 2022-12-27 13:51:53
tags: 
author: Rahul Jain
---

## Importance of Error Handling

Handling errors is an important part of being a good developer. Error handling is used to manage any unexpected behavior, to prevent the crashes, data loss and overall to run the code in a way we want it to run.

If an error occurs, it can cause the program to terminate unexpectedly, If we handle the errors properly, it can imporve the stability of the program and make it more reliable. And if any error occurs, it is way easier to handle and fix it.

Using error handling, we can also show user-friendly messages for the errors that we already know might happen, like a 404 page.
Consider this simple example where this function returns a character at a given index in a string.

    function charAt(str, index) { 
        return str.charAt(index) 
    }
    const product = charAt('abc',1)
    console.log(product)

But what if we pass a number or a boolean

    function length(str, index) {
        return str.charAt(index) // TypeError: str.charAt is not a function
    }
    const product = length(true,1)
    console.log(product) 

It throws an type error, this can be handled easily by checking the type of argument and if it is not a string we can show a message saying input must be string

    function length(str, index) {
        if(typeof str === 'string') {
            return str.charAt(index)
        } else {
            console.log("The input must be a string") // The input must be a string
        }
    }
    const product = length(true,1)
    console.log(product) 

With this check our program won't accept anything but string.

## Error handling in an asynchronus program

Error handling becomes even more important in asynchronous program, because there might be more unexpected behaviour compared to synchronous code. Asynchronous code is complex and hence more error prone.

In asynchronous code, it is important to structure your code in such a way that if an error occurs, none of the next functions that depend on the success of the current function should execute in callback.

### Callbacks

Error handling in asynchronous code normally follows a convention called "error first" callbacks. You simply pass error first then the data in the callback function, as shown in the example below.

    const fs = require('fs')
    function readFile(callbackFn) {
        fs.readFile('abc.txt', 'utf-8', (err, data) => {
            if(err) {
                callbackFn(err)
            } else {
                callbackFn(null, data)
            }
        })
    }
    readFile((err, data) => {
        if(err) {
            console.error(err)
        } else {
            console.log(data)
        }
    })

### Promises

In promises, errors can be handled using .catch() funtion or the try and catch block. Unlike callbacks we don't have to define a check for error in every call, we just need to declare it one it for it to work.

    const fs = require('fs')
    function readFile() {
        return new Promise((resolve, reject) => {
            fs.readFile('abc.txt', 'utf-8', (err, data) => {
                if(err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }
    
    readFile().then((data) => {
        console.log(data)
    })
    .catch((err) => {
        console.error(err)
    })

Using try and catch

    readFile().then((data) => {
        try {
            console.log(data)
        } catch (error) {
            console.error(error)
        }
    })
    .catch((err) => {
        console.error(err)
    })

It is very important to check for errors in your code, as it might reduce the chances of bugs and save you a lot of future work.

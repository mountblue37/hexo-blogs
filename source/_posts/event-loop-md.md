---
title: Event Loop
date: 2023-01-09 17:49:22
tags:
author: Rahul Jain
---

## What is the event loop

The event loop is a mechanism that allows JavaScript to perform non-blocking I/O operations. It works by continuously checking a list of events that are waiting to be processed and executing them when it is appropriate. This allows the program to run concurrently with other tasks and not be blocked by long-running operations, improving the overall performance and responsiveness of the program.

## How does it work

![event loop](https://miro.medium.com/max/752/1*7GXoHZiIUhlKuKGT22gHmA.png)

The event first will be sent to callstack and if the event is asynchronous it will be sent to Web API in browser or C++ API in NodeJs. If the event is synchronous it will stay in callstack and will be executed. It won't let any other events execute after it unless it's finished executing. While this is happening the Web API/C++ API are working in background and once they are finished processing the asynchronous event they will send it to callback queue. Where event loop will wait for the callstack to be emptied and once the callstack is empty it will send those completed asynchronos event to callstack and execute them one by one

Example :

    console.log("Hello") // Executes first 

    fs.readFile('abc.json', 'utf-8', (err, data) => {
        if(err) {
            console.error(err)
        } else {
            console.log("Inside async function") // Executes last
        }
    })

    console.log("Bye") // Executes second
